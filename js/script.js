/* -------------------------------------------------------------------------- */
/*                                UI Controller                               */
/* -------------------------------------------------------------------------- */

(function () {
    $(document).ready(function () {
        let DOMString;
        DOMString = {
            accordionTrigger: ".accordion__trigger",
            accordionOpened: "accordion__trigger--opened",
            accordionPanel: ".accordion__items-panel",
            modal: ".modal",
            modalClose: ".modal__close",
            aboutUsModal: "#about-us-modal",
            modalBox: ".modal__box",
            modalClosing: "modal--closing",
            clickHereBtn: "#click-here-btn",
            cta: ".cta",
            flippedCard: "flip-card--flipped",
            popupTrigger: ".js-popup",
            popup: ".popup",
            popupShow: "popup--show",
            popupHide: "popup--hide",
            popupContent: ".popup__content",
            containerFluid: ".container-fluid",
            containerFluidZoomIn: "container-fluid--zoom-in",
            containerFluidZoomOut: "container-fluid--zoom-out",
            imgElement: '<img src="img%src" alt="img%alt" class="img%class" />',
            imgSizeCover: "img__size--cover",
            sideBarRightTrigger: ".sidebar-right__trigger",
            sidebarRight: ".sidebar-right",
            sidebarRightShow: "sidebar-right--show",
            sidebarRightHide: "sidebar-right--hide",
            footer: "footer",
            footerShow: "footer--show",
            sidebarRightBg: "sidebar-right__resp-backgroud",
            sidebarRightBgHide: "sidebar-right__resp-backgroud--hide",
            sideBarLeftTrigger: ".sidebar-left__trigger",
            sidebarLeft: ".sidebar-left",
            sidebarLeftShow: "sidebar-left--show",
            sidebarLeftHide: "sidebar-left--hide",
            sidebarLeftBg: "sidebar-left__resp-backgroud",
            sidebarLeftBgHide: "sidebar-left__resp-backgroud--hide",
        };

        /* -------------------------------------------------------------------------- */
        /*                               Accodion Effect                              */
        /* -------------------------------------------------------------------------- */

        $(DOMString.accordionTrigger).click(function () {
            let thisAccordionPannel;
            thisAccordionPannel = $(this).next(DOMString.accordionPanel);
            $(this).toggleClass(DOMString.accordionOpened);

            thisAccordionPannel.slideToggle();

            $(DOMString.accordionTrigger).not(this).removeClass(DOMString.accordionOpened);
            $(DOMString.accordionPanel).not(thisAccordionPannel).slideUp();
        });

        /* -------------------------------------------------------------------------- */
        /*                                 Modal Effet                                */
        /* -------------------------------------------------------------------------- */

        $(`${DOMString.aboutUsModal} > ${DOMString.modalBox}`).click(function (e) {
            e.stopPropagation();
        });

        /* ----------------------------- Modal Event Receivers ----------------------------- */

        $(DOMString.clickHereBtn).click(openModal);
        $(DOMString.modalClose).click(closeModal);
        $(DOMString.modal).click(closeModal);

        /* ----------------------------- Modal Event Handlers ----------------------------- */

        function openModal(e) {
            $(DOMString.cta).addClass(DOMString.flippedCard);
            $(DOMString.aboutUsModal).css("display", "flex");
        }
        function closeModal() {
            $(DOMString.aboutUsModal)
                .addClass(DOMString.modalClosing)
                .delay(500)
                .queue(function () {
                    $(DOMString.aboutUsModal).removeClass(DOMString.modalClosing).hide().dequeue();
                });
            $(DOMString.cta).removeClass(DOMString.flippedCard);
        }

        /* -------------------------------------------------------------------------- */
        /*                                Popup Effect                               */
        /* -------------------------------------------------------------------------- */
        $(DOMString.popupContent).click((e) => {
            e.stopPropagation();
        });
        $(DOMString.popup).click(closePopup);
        /* ----------------------------- Event Listener ----------------------------- */
        $(DOMString.popupTrigger).click((event) => {
            const srcAttr = event.target.getAttribute("src");
            const altAttr = "";
            const classAttr = DOMString.imgSizeCover;
            $(DOMString.popupContent).html(createImgElement(srcAttr, altAttr, classAttr));
            openPopup();
        });

        /* ------------------------------ Event Handler ----------------------------- */
        function openPopup() {
            $(DOMString.containerFluid).addClass(DOMString.containerFluidZoomOut);
            $(DOMString.popup).show().addClass(DOMString.popupShow);
        }

        function closePopup() {
            $(DOMString.containerFluid).removeClass(DOMString.containerFluidZoomOut).addClass(DOMString.containerFluidZoomIn);
            $(DOMString.popup)
                .removeClass(DOMString.popupShow)
                .addClass(DOMString.popupHide)
                .delay(500)
                .queue(() => {
                    $(DOMString.containerFluid).removeClass(DOMString.containerFluidZoomIn);
                    $(DOMString.popup).removeClass(DOMString.popupHide).hide().dequeue();
                });
        }

        function createImgElement(srcAttr, altAttr = "", classAttr = "") {
            const element = DOMString.imgElement
                .replace("img%src", srcAttr)
                .replace("img%alt", altAttr)
                .replace("img%class", classAttr);

            return element;
        }

        /* -------------------------------------------------------------------------- */
        /*                              SideBars Effects                              */
        /* -------------------------------------------------------------------------- */
        [DOMString.sidebarLeft, DOMString.sidebarRight].forEach((sidebarSelector) => {
            $(sidebarSelector).click((e) => {
                e.stopPropagation();
            });
        });

        /* ----------------------------- Event Listener ----------------------------- */
        [DOMString.sideBarLeftTrigger, DOMString.sideBarRightTrigger].forEach((sidebarTriggerSelector) => {
            $(sidebarTriggerSelector).click((e) => {
                e.stopPropagation();
                showSideBar(sidebarTriggerSelector);
            });
        });

        /* ------------------------------ Events Handlers ----------------------------- */
        function getSidebarsSelectors(sidebarTriggerSelector) {
            const selectors = {};
            if (sidebarTriggerSelector == DOMString.sideBarRightTrigger) {
                selectors.sidebarBg = DOMString.sidebarRightBg;
                selectors.sidebarBgHide = DOMString.sidebarRightBgHide;
                selectors.sidebar = DOMString.sidebarRight;
                selectors.sidebarShow = DOMString.sidebarRightShow;
                selectors.sidebarHide = DOMString.sidebarRightHide;
            } else {
                selectors.sidebarBg = DOMString.sidebarLeftBg;
                selectors.sidebarBgHide = DOMString.sidebarLeftBgHide;
                selectors.sidebar = DOMString.sidebarLeft;
                selectors.sidebarShow = DOMString.sidebarLeftShow;
                selectors.sidebarHide = DOMString.sidebarLeftHide;
            }

            return selectors;
        }

        function showSideBar(trigger) {
            // Get selectors
            const selectors = getSidebarsSelectors(trigger);

            // Add the black-transparent Background elemen
            const backgroudElement = `<div class="${selectors.sidebarBg}"></div>`;
            $(selectors.sidebar).before(backgroudElement);

            // Show the hiddenSidebar upon the background defined above, this gonna perfom the sliding animation effect
            $(selectors.sidebar).addClass(selectors.sidebarShow);
            trigger == DOMString.sideBarRightTrigger ? $(DOMString.footer).addClass(DOMString.footerShow) : "";

            // Append the click event to the newly created backgroud element
            $(`.${selectors.sidebarBg}`).click((e) => {
                hideSideBar(trigger);
            });
        }

        function hideSideBar(trigger) {
            // Get selectors
            const selectors = getSidebarsSelectors(trigger);

            //Animate and Hide the Sidebar
            $(selectors.sidebar)
                .addClass(selectors.sidebarHide)
                .delay(500)
                .queue(() => {
                    $(selectors.sidebar).removeClass(selectors.sidebarShow).removeClass(selectors.sidebarHide).dequeue();
                });

            // Remove the background
            trigger == DOMString.sideBarRightTrigger ? $(DOMString.footer).removeClass(DOMString.footerShow) : "";
            $(`.${selectors.sidebarBg}`).addClass(selectors.sidebarBgHide).delay(500).remove();
        }
    });
})();
